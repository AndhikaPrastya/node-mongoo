const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const connectDB = require('./helpers/DBUtil');
const BodyParser = require('body-parser');
const route = require ("./routes/index");

const app = express();
const port = process.env.PORT;
connectDB();

app.use(BodyParser.json({ limit: "50mb" }));
app.use(BodyParser.urlencoded({ extended: true, limit: "50mb" }));
app.use('/api/v1', route);

//listen
app.listen(port, ()=>{
    console.log(`Server running in port ${port}`)
});

module.exports = app