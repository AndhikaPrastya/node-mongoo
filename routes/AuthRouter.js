const express = require("express");
const AuthController = require("../controllers/AuthController");
const ctrl = new AuthController();
const authRouter = express.Router();

authRouter.post('/register',  ctrl.register);
authRouter.post('/login', ctrl.login);

module.exports = authRouter