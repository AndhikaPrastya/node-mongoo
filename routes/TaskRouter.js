const express = require("express");
const {JwtFilter} = require('../helpers/JwtUtil');
const TaskController = require("../controllers/TaskController");
const ctrl = new TaskController();
const taskRouter = express.Router();

taskRouter.all("/*", JwtFilter);

taskRouter.get('/', ctrl.getAllTask);
taskRouter.get('/:id', ctrl.getTaskById);
taskRouter.post('/', ctrl.createTask);
taskRouter.put('/', ctrl.updateTask);
taskRouter.delete('/:id', ctrl.deleteTask);

module.exports = taskRouter