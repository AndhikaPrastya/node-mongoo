const { Router } = require('express')
const authRouter = require('./AuthRouter');
const taskRouter = require('./TaskRouter');

const routes = Router()
routes.use('/auth', authRouter)
routes.use('/task', taskRouter)

module.exports = routes;