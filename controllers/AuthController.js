const User = require('../models/UserModel')
const bcrypt = require('bcrypt');
const {registerValidation, loginValidation} = require("../schemas/AuthSchema");
const {createJwtToken} = require('../helpers/JwtUtil');

class AuthController {
    async register(req, res) {
        
        try {
            const {error} = registerValidation(req.body);
            if(error) return res.status(400).send({
                'status': 400,
                'msg' : error.details[0].message,
            });

            const user = new User(req.body);
            const emailExist = await User.findOne({email: user.email});
            if(emailExist) return res.status(400).send({
                'status': 400,
                'msg' : 'Email already registered',
            });

            //Hash password
            const encPassword = await hashPassword(req.body.password);
            user.password = encPassword;

            const saveRegister = await user.save();
            res.status(200).send({
                'status': 200,
                'msg' : 'Successfully',
                'data': saveRegister
            });
        } catch (err) {
            res.status(500).send({
                'status': 500,
                'msg' : 'Internal Server Error',
                'error': err
            });
        }
    }

    async login(req, res) {

        try {
            const {error} = loginValidation(req.body);
            if(error) return res.status(400).send({
                'status': 400,
                'msg' : error.details[0].message,
            });

            const user = await User.findOne({email: req.body.email});
            if(!user) return res.status(404).send({
                'status': 404,
                'msg' : 'Incorrect email or password',
            });

            //Validation password
            const valPassword = await validationPassword(req.body.password, user.password);
            if(!valPassword) return res.status(404).send({
                'status': 404,
                'msg' : 'Incorrect email or password',
            });

            let data = {
                _id : user._id,
                name : user.name,
            }

            data.token = await createJwtToken(data);
            res.status(200).send({
                'status': 200,
                'msg' : 'Successfully',
                'data': data
            });
        } catch (err) {
            res.status(500).send({
                'status': 500,
                'msg' : 'Internal Server Error',
                'error': err
            });
        }
    }

}

const hashPassword = async (plainText) => {
    const salt = await bcrypt.genSalt(parseInt(process.env.SALT));
    const password = await bcrypt.hash(plainText, salt);
    return password;
}

const validationPassword = async (encPassword, inputPassword) => {
    const match = await bcrypt.compare(encPassword, inputPassword);
    return match;
}

module.exports = AuthController