const Task = require('../models/TaskModel');
const {inputTaskValidation, updateTaskValidation, getIdValidation} = require("../schemas/TaskSchema");

class TaskController {

    async getAllTask(req, res) {
        let userId = req.app.locals.id

        try {
            const task = await Task.find({user: userId});
            res.status(200).send({
                'status': 200,
                'msg' : 'Successfully',
                'data': task
            });
        } catch (err) {
            res.status(500).send({
                'status': 500,
                'msg' : 'Internal Server Error'
            });
        }
    }

    async getTaskById(req, res) {
        let userId = req.app.locals.id

        try {
            const {error} = getIdValidation({id: req.params.id});
            if(error) return res.status(400).send({
                'status': 400,
                'msg' : error.details[0].message
            });

            const task = await Task.findOne({_id: req.params.id, user: userId});
            if(task == null) return res.status(404).send({
                'status': 404,
                'msg' : 'Data Not Found'
            });

            res.status(200).send({
                'status': 200,
                'msg' : 'Successfully',
                'data': task
            });
        } catch (err) {
            console.log(err)
            res.status(500).send({
                'status': 500,
                'msg' : 'Internal Server Error'
            });
        }
    }

    async createTask(req, res) {
        let userId = req.app.locals.id
        
        try {
            const {error} = inputTaskValidation(req.body);
            if(error) return res.status(400).send({
                'status': 400,
                'msg' : error.details[0].message
            });

            const task = new Task(req.body)
            task.user = userId
            
            const saveTask = await task.save();
            return res.status(200).send({
                'status': 200,
                'msg' : 'Successfully',
                'data': saveTask
            });
        } catch (err) {
            return res.status(500).send({
                'status': 500,
                'msg' : 'Internal Server Error',
                'error': err
            });
        }
    }

    async updateTask(req, res) {
        let userId = req.app.locals.id

        try {
            req.body.user = userId
            const {error} = updateTaskValidation(req.body);
            if(error) return res.status(400).send({
                'status': 400,
                'msg' : error.details[0].message
            });

            const taskExist = await Task.findOne({_id: req.body.id, user: userId});
            if(taskExist == null) return res.status(404).send({
                'status': 404,
                'msg' : 'Data Not Found'
            });

            const updateTask = await Task.updateOne({_id: req.body.id}, {$set: req.body});
            return res.status(200).send({
                'status': 200,
                'msg' : 'Successfully',
                'data': updateTask
            });
        } catch (err) {
            return res.status(500).send({
                'status': 500,
                'msg' : 'Internal Server Error',
                'error': err
            });
        }
    }

    async deleteTask(req, res) {
        let userId = req.app.locals.id

        try {
            const {error} = getIdValidation({id: req.params.id});
            if(error) return res.status(400).send({
                'status': 400,
                'msg' : error.details[0].message
            });

            const task = await Task.findOne({_id: req.params.id, user: userId});
            if(task == null) return res.status(404).send({
                'status': 404,
                'msg' : 'Data Not Found'
            });

            const deleteTask = await Task.deleteOne({_id: req.params.id});
            res.status(200).send({
                'status': 200,
                'msg' : 'Successfully',
                'data': deleteTask
            });
        } catch (err) {
            res.status(500).send({
                'status': 500,
                'msg' : 'Internal Server Error',
                'error': err
            });
        }
    }

}

module.exports = TaskController