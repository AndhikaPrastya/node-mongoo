const mongoose = require('mongoose');
let URI = process.env.DB_URI_TEST;

if(process.env.NODE_ENV == 'development') URI = process.env.DB_URI_DEVELOPMENT;

const connectDB = async () => {
    try {
        await mongoose.connect(URI);
    } catch (error) {
        console.log(error)
    }
}

module.exports = connectDB;