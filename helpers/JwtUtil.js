const jwt = require('jsonwebtoken');
const secret = process.env.JWT_SECRET

const createJwtToken = async (data) => {
    let expires = process.env.JWT_EXPIRES;
    return jwt.sign(data, secret, {  expiresIn : expires});
}

const getToken = (bearer) => {
    return bearer.slice(7, bearer.length)
}

const JwtFilter = (req, res, next) => {
    if(req.headers.authorization) {
        const token = getToken(req.headers.authorization)

        try {
            let decoded = jwt.verify(token, secret);
            req.app.locals.id = decoded._id
            req.app.locals.name = decoded.name
            next();
        } catch (error) {
            return res.status(401).send({
                'status': 401,
                'msg': 'Token is not valid'
            })
        }
    } else {
        return res.status(401).send({
            'status': 401,
            'msg': 'Token is missing'
        })
    }
}


module.exports = {createJwtToken, JwtFilter};