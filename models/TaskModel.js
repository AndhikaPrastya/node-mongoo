const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaskSchema = new Schema ({
        name: { type: String, required: true },
        desc: { type: String, required: true },
        status: { type: String, default: 'Open' },
        user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
        createdAt: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Task', TaskSchema)