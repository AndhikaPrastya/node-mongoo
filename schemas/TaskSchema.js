const Joi = require('joi');

const inputTaskValidation = (data) => {
    const schema = Joi.object({
        name: Joi.string().required(),
        desc: Joi.string().required(),
        status: Joi.string(),
    })

    return schema.validate(data);
}

const updateTaskValidation = (data) => {
    const schema = Joi.object({
        id: Joi.string().min(24).max(24).required(),
        name: Joi.string().required(),
        desc: Joi.string().required(),
        user: Joi.string().required(),
        status: Joi.string().required(),
    })

    return schema.validate(data);
}

const getIdValidation = (data) => {
    const schema = Joi.object({
        id: Joi.string().min(24).max(24).required(),
    })

    return schema.validate(data);
}


module.exports = {inputTaskValidation, getIdValidation, updateTaskValidation};