const request = require('supertest');
const app = require("../index");


// let id;
// let reqUser
// beforeAll(async () => {
//   let user = {
//       nama : "Dhika 777",
//       alamat : "Jl.Getasan Semarang",
//       email : "dhika@gmail.com",
//       phone : "085869595134",
//       password : "12345",
//       depart: "6375c0be0f54920c28d25f31"
//   };
//   const res = await request(app)
//     .post("/api/v1/users")
//     .send(user);
//     reqUser = user
//     id = res.body.data._id;
// });

describe("Auth API",  () => {
  test("REGISTER", async () => {
    let data = {
      name : 'Test',
      address : 'Test Address',
      email : "112233@gmail.com",
      password : "123456"
    }
    const res = await request(app).post('/api/v1/auth/register').send(data);
    // console.log(res)
    //res status code
    expect(res.statusCode).toBe(200);
    //res body
    // expect(res.body).toHaveProperty('status');
    // expect(res.body).toHaveProperty('msg');
    // expect(res.body).toHaveProperty('data');
    // expect(res.body.status).toBe(200);
    // expect(res.body.msg).toBe('Successfully');
    //res body data
    // Array.from(res.body.data).forEach((e) => {
    //   expect(Object.keys(e)).toHaveLength(9);
    //   expect(e).toHaveProperty("_id");
    //   expect(e).toHaveProperty("nama");
    //   expect(e).toHaveProperty("alamat");
    //   expect(e).toHaveProperty("email");
    //   expect(e).toHaveProperty("phone");
    //   expect(e).toHaveProperty("password");
    //   expect(e).toHaveProperty("depart");
    //   expect(e).toHaveProperty("createdAt");
    //   expect(e).toHaveProperty("__v");
    // });
  })

  // test("GET BY ID", async () => {
  //   const res = await request(app).get(`/api/v1/users/${id}`);
    
  //   //res status code
  //   expect(res.statusCode).toBe(200);
  //   //res body
  //   expect(res.body).toHaveProperty('status');
  //   expect(res.body).toHaveProperty('msg');
  //   expect(res.body).toHaveProperty('data');
  //   expect(res.body.status).toBe(200);
  //   expect(res.body.msg).toBe('Successfully');
  //   //res body data
  //   expect(res.body.data._id).toBe(id);
  //   expect(res.body.data.nama).toBe(reqUser.nama);
  //   expect(res.body.data.alamat).toBe(reqUser.alamat);
  //   expect(res.body.data.email).toBe(reqUser.email);
  //   expect(res.body.data.phone).toBe(reqUser.phone);
  //   expect(res.body.data.password).toBe(reqUser.password);
  // })

  // let idPost
  // test("POST", async () => {
  //   // let idPost
  //   const res = await request(app).post('/api/v1/users').send({
  //     nama : "Dhika 8888",
  //     alamat : "Jl.Getasan Semarang",
  //     email : "dhika@gmail.com",
  //     phone : "085869595134",
  //     password : "12345",
  //     depart: "6375c0be0f54920c28d25f31"
  //   });

  //   idPost = res.body.data._id 

  //   //res status code
  //   expect(res.statusCode).toBe(201);
  //   //res body
  //   expect(res.body).toHaveProperty('status');
  //   expect(res.body).toHaveProperty('msg');
  //   expect(res.body).toHaveProperty('data');
  //   expect(res.body.status).toBe(201);
  //   expect(res.body.msg).toBe('Successfully');
  //   //res body data
  //   expect(res.body.data).toMatchObject({
  //     nama : "Dhika 8888",
  //     alamat : "Jl.Getasan Semarang",
  //     email : "dhika@gmail.com",
  //     phone : "085869595134",
  //     password : "12345",
  //     depart: "6375c0be0f54920c28d25f31"
  //   })

  //   // // Check the response
  //   // expect(response.body._id).toBeTruthy();
  //   // expect(response.body.title).toBe(data.title);
  //   // expect(response.body.content).toBe(data.content);

  //   // // Check data in the database
  //   // const post = await Post.findOne({ _id: response.body._id });
  //   // expect(post).toBeTruthy();
  //   // expect(post.title).toBe(data.title);
  //   // expect(post.content).toBe(data.content);
  // })

  // test("PUT", async () => {
  //   const res = await request(app).put(`/api/v1/users/${idPost}`).send({
  //     nama : "Dhika 999",
  //     alamat : "Jl.Getasan Semarang Jawa Tengh",
  //     email : "dhika22@gmail.com",
  //     phone : "085869595135",
  //     password : "12346",
  //     depart: "6375c0be0f54920c28d25f31"
  //   });

  //  //res status code
  //  expect(res.statusCode).toBe(200);
  //  //res body
  //  expect(res.body).toHaveProperty('status');
  //  expect(res.body).toHaveProperty('msg');
  //  expect(res.body).toHaveProperty('data');
  //  expect(res.body.status).toBe(200);
  //  expect(res.body.msg).toBe('Successfully');
  // //  //res body data
  // //  expect(res.body.data).toMatchObject({
  // //    nama : "Dhika 8888",
  // //    alamat : "Jl.Getasan Semarang",
  // //    email : "dhika@gmail.com",
  // //    phone : "085869595134",
  // //    password : "12345",
  // //    depart: "6375c0be0f54920c28d25f31"
  // //  })
  // })

  // test("DELETE", async () => {
  //   const res = await request(app).delete(`/api/v1/users/${idPost}`);

  //   //res status code
  //   expect(res.statusCode).toBe(200);
  //   //res body
  //   expect(res.body).toHaveProperty('status');
  //   expect(res.body).toHaveProperty('msg');
  //   expect(res.body.status).toBe(200);
  //   expect(res.body.msg).toBe('Successfully');
  // })

})
